#include <stdio.h>
 
int main(){
   
    const float PI = 3.1415f;
    float radio,d,p,a;
    
    printf("Dame el radio de un circulo\n");
    scanf("%f",&radio);
    getchar();
    
    d = 2.0f* radio;
    p = 2.0f*PI*radio;
    a = PI*(radio*radio);
    
    printf("Diametro = %0.1f\nPerimetro = %0.1f\nArea = %0.1f\n",d,p,a);
      
    getchar();
    return 0;
}