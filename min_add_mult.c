#include <stdio.h>
#include "min_add_mult.h"

 
int main () {

   /* local variable definition */
   int a = 100;
   int b = 200;
   int ret;
 
   /* calling a function to get max value */
   ret = max(a, b); 
   printf( "Max value is : %d\n", ret );
   
   /* calling a function to get min value */
   ret = min(a, b); 
   printf( "Min value is : %d\n", ret );
   
   /* calling a function to get sum value */
   ret = add(a, b); 
   printf( "Sum value is : %d\n", ret );
   
   /* calling a function to get mul value */
   ret = mul(a, b); 
   printf( "Mult value is : %d\n", ret );
 
   printSalida();
   
   getchar();
   return 0;
}
