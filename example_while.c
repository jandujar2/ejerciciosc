#include <stdio.h>

int main(){
    
    int n;
    int i;

    printf("dame un numero del 1 al 10 y te dire su tabla de multiplicar\n");
    scanf("%d", &n);
    getchar();

    i=1;
    while(i<=10){
      printf("%dx%d = %d\n",n,i,n*i);
      i = i +1; // i++;    
    }
   
    getchar();
    
    return 0;
}