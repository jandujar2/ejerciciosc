#include <stdio.h>

int main(){
    
    int number;

    printf("dame un numero\n");
    scanf("%d", &number);
    getchar();

    switch(number%2){
        case 0:
            printf("Es un numero par\n");
            break;
        case 1:
            printf("Es un numero impar\n");
            break;
    }
    
    /*
    if(number%2 == 0){
        printf("Es un numero par\n");
    }else{
        printf("Es un numero impar\n");
    }
    */
    
    switch(number){
        case 0:
            printf("Es el numero 0\n");
            break;
        case 1:
            printf("Es el numero 1\n");
            break;
        default:
            printf("No es ni 0 ni 1\n");
            break;
    }
    
    getchar();
    

    return 0;
}