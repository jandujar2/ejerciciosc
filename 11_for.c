#include <stdio.h>

int main(){
    
    int numero1;
    int numero2;
    int multiplicacion=0;
  
    printf("Dame un numero\n");
    scanf("%d", &numero1);
    getchar();
    
    printf("Dame otro numero\n");
    scanf("%d", &numero2);
    getchar();

    for(int contador=0; contador<numero2;contador++){
        multiplicacion+=numero1;
    }
    
    /*
    contador=0;
    while(contador<numero2){
        multiplicacion+=numero1;
        
        contador++;
    }
    */
    
    printf("Resultado = %dx%d = %d\n",numero1,numero2,multiplicacion);
   
    getchar();
    
    return 0;
}