/* function declaration */
int max(int num1, int num2);
int min(int num1, int num2);
int add(int num1, int num2);
int mul(int num1, int num2);
void printSalida();

/* function returning the max between two numbers */
int max(int num1, int num2) {

   /* local variable declaration */
   int result;
 
   if (num1 > num2)
      result = num1;
   else
      result = num2;
 
   return result; 
}

int min(int num1, int num2) {
   if (num1 > num2){
      return num2;
   }else{
      return num1;
   }
}

int add(int num1, int num2) {
   int salida = num1 + num2;
   return salida;
}

int mul(int num1, int num2) {
   return num1 * num2;
}

void printSalida(){
    printf("Gracias por venir a clase\n");
}
