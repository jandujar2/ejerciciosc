#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

void header();

enum suit {
    club = 0,
    diamonds = 10,
    hearts = 20,
    spades = 3
};

typedef enum t_bebidas {
    agua,
    cocacola = 10,
    zumo,
    redbull
} bebidas;

typedef enum t_colores{
    rojo,
    azul,
    verde
} colores;

typedef struct t_book {
   char  title[50];
   int   book_id;
} book; 


int main(){
    int i, n;
    time_t t;
    
    int randoms[5];
    
    
    header();    
   
    n = 5;
   
    /* Intializes random number generator */
    time(&t);
    srand((unsigned) t);

    /* Print 5 random numbers from 0 to 49 */
    for( i = 0 ; i < n ; i++ ) {
      randoms[i] = rand() % 50;
      printf("%d\n", randoms[i]);
    }

    printf("%d\n", -3 + rand()%4);
    
    
    bebidas beb;
    
    beb = agua;
    
    printf("Has escogido %d\n",beb);

    colores col = rojo;
    
    switch(col){
        case rojo:
            printf("Rojo\n");
            break;
        case verde:
            printf("Verde\n");
            break;
        case azul:
            printf("Azul\n");
            break;
    }
    
    book book1;
    strcpy( book1.title, "Ejemplo de titulo");
    /* Equivalente a  ->
    book1.title[0] = 'E';
    book1.title[1] = 'j';
    book1.title[2] = 'e';
    book1.title[3] = 'm';
    ..
    ..
    ..
    book1.title[10] = '\0';
    */
    
    
    book1.book_id = 12;
    
    printf("Libro %s\ncon id = %d\n",book1.title, book1.book_id);
    
    getchar();
    return 0;
}


void header(){
    printf("===================\n");
    printf("Examen Programacion\n");
    printf("===================\n");
}