#include <stdio.h> //Librería estandard input output

//Punto de entrada de nuestro programa
int main()
{
    int num = 0;
    
    printf("/*****************************************/\n");
    printf("/**********F I N A L   D O O R S**********/\n");
    printf("/*****************************************/\n");
    
    printf("Tras mas de 84 horas de juego y conseguir vencer al increible dragon de 12 cabezas\n");
    printf("en sus 7 transformaciones, te encuentras frente la ultima prueba del juego.");
    
    getchar();
    system("cls");
    system("color B0"); //Cambia el color de fondo (1) y de letra (0)
    system("dir");
    
    
    printf("Ante ti hay 2 puertas, una lleva a riquezas infinitas...\n");
    printf("...la otra, es una trampa mortal que te borra la partida y te obliga\n");
    printf("a volver a empezar desde cero.");
    
    getchar();
    system("cls");
    system("color 0E");
    
    printf("Que puerta escoges? La 1 o la 2: ");
    scanf("%i", &num);
    
    getchar();
    system("cls");
    system("color 4C");
    
    if(num == 1)
    {
        printf("Deberias haber escogido la otra puerta...\n\n");
    }
    else if(num == 2)
    {
        printf("Deberias haber escogido la otra puerta 2...\n\n");
    }
    else
    {
        printf("Vaya, escogiste la puerta equivocada...\n\n");
    }
    
    printf("Lo has perdido todo...\n");
    printf("TU PARTIDA HA SIDO BORRADA...\n");
    printf("Suerte la proxima vez... ;)\n\n");
    
    printf("GAME OVER\n\n");
    
    printf("Pulsa ENTER para cerrar.\n");
    getchar();
    
    return 0;
}