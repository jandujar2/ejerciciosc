/*******************************************************************************************
*
*   raylib [core] example - Keyboard input
*
*   This example has been created using raylib 1.0 (www.raylib.com)
*   raylib is licensed under an unmodified zlib/libpng license (View raylib.h for details)
*
*   Copyright (c) 2014 Ramon Santamaria (@raysan5)
*
********************************************************************************************/

#include "raylib.h"

//typedef vector2_t{
//    int x;
//    int y;
//}Vector2;

#define VELOCIDAD 5.0f
#define RADIO 100

int main()
{
    // Initialization
    //--------------------------------------------------------------------------------------
    int screenWidth = 800;
    int screenHeight = 800;
    int framesCounter = 0;
  
    InitWindow(screenWidth, screenHeight, "raylib [core] example - keyboard input");
    
    Vector2 ballPosition = { (float)screenWidth/2, (float)screenHeight/2 };

    SetTargetFPS(60);       // Set target frames-per-second
    //--------------------------------------------------------------------------------------   
    
    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update
        //----------------------------------------------------------------------------------
        if (IsKeyDown(KEY_RIGHT)) ballPosition.x += VELOCIDAD;
        if (IsKeyDown(KEY_LEFT)) ballPosition.x -= VELOCIDAD;
        if (IsKeyDown(KEY_UP)) ballPosition.y -= VELOCIDAD;
        if (IsKeyDown(KEY_DOWN)) ballPosition.y += VELOCIDAD;
        //----------------------------------------------------------------------------------

        framesCounter++;

        if (framesCounter > 60)
        {
            ballPosition.x+=VELOCIDAD;
            framesCounter = 0;
        }
        
        if(ballPosition.x<RADIO){
            ballPosition.x = RADIO;
        }else if(ballPosition.x>screenWidth-RADIO){
            ballPosition.x = screenWidth-RADIO;
        }
        
        if(ballPosition.y<RADIO){
            ballPosition.y = RADIO;
        }else if(ballPosition.y>screenHeight-RADIO){
            ballPosition.y = screenHeight-RADIO;
        }
        
        
        
        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();

            ClearBackground(RAYWHITE);

            DrawCircleV(ballPosition, RADIO, MAROON);
            
            DrawText("move the ball with arrow keys", 10, 10, 20, DARKGRAY);
            DrawText(FormatText("(%d,%d)", (int)ballPosition.x, (int)ballPosition.y), screenWidth -100, 10, 20, MAROON);          

        EndDrawing();
        //----------------------------------------------------------------------------------
    }

    // De-Initialization
    //--------------------------------------------------------------------------------------
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------

    return 0;
}